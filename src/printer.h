#pragma once

#include "board.h"
#include "save_manager.h"

class Printer
{
    inline static bool unicode = true;
    inline static bool space = false;

public:
    // Set preferences for printing, in case your terminal won't show certain characters
    static void set_preferences();
    inline static void set_use_unicode(bool value) { unicode = value; }
    inline static void set_use_extra_space(bool value) { space = value; }

    // Prints the entire board, with White being on the lower side
    static void print_board(const Board &board);

    // Print list of available saves
    static void print_save_list(const std::vector<SaveInfo> &saves);
};
