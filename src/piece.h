#pragma once

#include <vector>
#include <unordered_set>
#include "position.h"

class Board;

class Piece
{
    int player = 0;

public:
    Piece() = default;
    Piece(int player) : player(player) {}
    virtual ~Piece() {}

    virtual int get_player() const { return player; }
    virtual std::unordered_set<Position> get_all_legal_moves(const Board &board, const Position &position) const = 0;
};