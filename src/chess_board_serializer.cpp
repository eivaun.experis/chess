#include <vector>

#include "chess_board_serializer.h"
#include "chess.h"
#include "chess_pieces.h"

void ChessBoardSerializer::set_serialization_target(const ChessBoard *board)
{
    this->board = board;
}

void ChessBoardSerializer::load_serialization_data(ChessBoard &board)
{
    board.set_next_player((ChessColor)next_player);
    board.get_white() = white_castle;
    board.get_black() = black_castle;
    board.get_en_passant() = en_passant;
    for (auto &d : data)
    {
        board.add_piece(d.pos, std::move(d.piece));
    }
}
