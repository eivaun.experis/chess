#include "chess_board.h"
#include "chess.h"
#include "chess_pieces.h"
#include "cmath"

ChessBoard::ChessBoard() : Board(8, 8) {}

void ChessBoard::setup_board()
{
    // For each color, set up the pieces where they should be
    for (ChessColor color : {ChessColor::White, ChessColor::Black})
    {
        int y = color == ChessColor::White ? 1 : 6;
        for (int x = 0; x < 8; x++)
        {
            add_piece<Pawn>(Position(x, y), color);
        }

        y = color == ChessColor::White ? 0 : 7;
        add_piece<Rook>(Position(0, y), color);
        add_piece<Knight>(Position(1, y), color);
        add_piece<Bishop>(Position(2, y), color);
        add_piece<Queen>(Position(3, y), color);
        add_piece<King>(Position(4, y), color);
        add_piece<Bishop>(Position(5, y), color);
        add_piece<Knight>(Position(6, y), color);
        add_piece<Rook>(Position(7, y), color);
    }
}

std::unordered_set<Position> ChessBoard::get_legal_moves(const Position &start) const
{
    auto moves = Board::get_legal_moves(start);

    // Remove any moves leaving the king in check
    for (auto it = moves.begin(), last = moves.end(); it != last;)
    {
        if (move_is_check(start, *it, next_player))
            it = moves.erase(it);
        else
            ++it;
    }

    return moves;
}

bool ChessBoard::move_piece(const Position &start, const Position &goal)
{
    // If you're trying to move a non-existent piece or an enemy piece, return false
    if (get_piece(start) == nullptr || get_piece(start)->get_player() != next_player)
        return false;

    // Castling
    if (handle_castling(start, goal))
        return true;

    // En passant
    if (handle_en_passant(start, goal))
        return true;

    // Assuming it isn't some special move like castling or en passant, return false if this is an illegal move
    if (!check_if_legal_move(start, goal))
        return false;

    // Move your piece into the goal position
    get_ptr(goal) = std::move(get_ptr(start));

    // Clear en passant
    en_passant.erase(next_player);

    // Pawn promotion
    handle_promotion(goal);

    // If a pawn moved two spaces, note it down to be possible to en passant it
    Piece *piece = get_piece(goal);
    if (abs(start.y - goal.y) == 2 && get_piece_enum(piece) == ChessPiece::Pawn)
        en_passant[next_player] = Position(goal.x, goal.y - ((goal.y - start.y) / 2));

    // Note down any movement which might restrict your ability to castle in the future
    if (black_castle.count(start))
        black_castle.erase(black_castle.find(start));
    if (white_castle.count(start))
        white_castle.erase(white_castle.find(start));

    // Switch turns
    next_player = next_player == ChessColor::White ? ChessColor::Black : ChessColor::White;
    return true;
}

bool ChessBoard::is_check(ChessColor defending_player) const
{
    Position king_pos(-1, -1);
    ChessColor king_player = defending_player;

    // Find the king
    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            auto p = get_piece({x, y});
            if (p)
            {
                if (get_piece_enum(p) == ChessPiece::King && p->get_player() == king_player)
                {
                    king_pos = {x, y};
                }
            }
        }
    }

    // No king on the board
    if (king_pos.x == -1)
        return false;

    // Check if any enemy piece can attack the king
    return can_any_piece_attack(king_pos, king_player, false);
}

bool ChessBoard::is_checkmate(ChessColor defending_player) const
{
    Position king_pos(-1, -1);
    ChessColor king_player = defending_player;
    std::vector<Position> pieces;

    // Find the friendly pieces
    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            auto p = get_piece({x, y});
            if (p && p->get_player() == king_player)
            {
                pieces.push_back({x, y});

                if (get_piece_enum(p) == ChessPiece::King)
                    king_pos = {x, y};
            }
        }
    }

    // Not checkmate if you're not in check
    if (!can_any_piece_attack(king_pos, king_player, false))
        return false;

    // Check if any move is valid
    // For each piece
    for (const auto &pos : pieces)
    {
        // For each move
        auto moves = get_legal_moves(pos);
        for (const auto &move : moves)
        {
            // If any piece can make a move resulting in the king not being in check, it's not checkmate
            if (!move_is_check(pos, move, king_player))
                return false;
        }
    }

    // Check mate, as we did not find any move to resolve it
    return true;
}

bool ChessBoard::is_stalemate() const
{
    Position king_pos(-1, -1);
    std::vector<Position> pieces;

    // Find the friendly pieces
    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            auto p = get_piece({x, y});
            if (p && p->get_player() == next_player)
            {
                pieces.push_back({x, y});
                if (get_piece_enum(p) == ChessPiece::King)
                    king_pos = {x, y};
            }
        }
    }

    // Not stalemate if you're in check
    if (can_any_piece_attack(king_pos, next_player, false))
        return false;

    // Check if any move is valid
    // For each piece
    for (const auto &pos : pieces)
    {
        // For each move
        auto moves = get_legal_moves(pos);
        for (const auto &move : moves)
        {
            // If any piece can make a move resulting in the king not being in check, it's not checkmate
            if (!move_is_check(pos, move, next_player))
                return false;
        }
    }

    // Stalemate, as there's no valid moves
    return true;
}

bool ChessBoard::can_any_piece_attack(const Position &pos, ChessColor defending_player, bool check_if_check) const
{
    // For each position on the board
    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            // If it's an enemy piece
            auto p = get_piece({x, y});
            if (p && p->get_player() != defending_player)
            {
                // If we need to check if the move results in the attacking players king being in check
                if (check_if_check)
                {
                    if (check_if_legal_move({x, y}, pos))
                        return true;
                }
                else
                {
                    if (p->get_all_legal_moves(*this, {x, y}).count(pos))
                        return true;
                }
            }
        }
    }

    // No pieces could attack the position
    return false;
}

bool ChessBoard::move_is_check(const Position &start, const Position &goal, ChessColor defending_player) const
{
    // Removing the const qualifier. Potentially dangerous, but we'll make sure to revert any changes we make.
    ChessBoard &cb = const_cast<ChessBoard &>(*this);

    // Move the piece, saving the possible taken piece
    std::unique_ptr<Piece> taken = std::move(cb.get_ptr(goal));
    cb.get_ptr(goal) = std::move(cb.get_ptr(start));

    // Check is the king is in check
    bool check = is_check(defending_player);

    // Put the pieces back
    cb.get_ptr(start) = std::move(cb.get_ptr(goal));
    cb.get_ptr(goal) = std::move(taken);

    return check;
}

void ChessBoard::set_promotion_prompt(std::function<ChessPiece()> promotion_prompt)
{
    this->promotion_prompt = promotion_prompt;
}

bool ChessBoard::handle_castling(const Position &start, const Position &goal)
{
    ChessColor color = next_player;
    for (int i = 0; i < 8; i += 7)
    {
        // Check if everything is setup up to castle king-side
        if (start == Position(4, i) &&
            goal == Position(6, i) &&
            ((black_castle.count(Position(7, i)) &&
              black_castle.count(start)) ||
             (white_castle.count(Position(7, i)) &&
              white_castle.count(start))) &&
            get_piece(Position(5, i)) == nullptr &&
            get_piece(Position(6, i)) == nullptr &&
            get_piece_enum(get_piece(Position(start))) == ChessPiece::King &&
            get_piece_enum(get_piece(Position(7, i))) == ChessPiece::Rook)
        {
            // Remove old pieces
            remove_piece(Position(4, i));
            remove_piece(Position(7, i));
            // Add in new pieces
            add_piece<King>(Position(6, i), color);
            add_piece<Rook>(Position(5, i), color);
            // Prevent future castling from happening
            if (black_castle.count(start))
                black_castle.erase(black_castle.find(start));
            if (white_castle.count(start))
                white_castle.erase(white_castle.find(start));
            // Clear any possible en passant from last round
            en_passant.erase(next_player);
            // Switch turn
            next_player = next_player == ChessColor::White ? ChessColor::Black : ChessColor::White;
            return true;
        }
        // Check if everything is setup up to castle queen-side
        if (start == Position(4, i) &&
            goal == Position(1, i) &&
            ((black_castle.count(Position(0, i)) &&
              black_castle.count(start)) ||
             (white_castle.count(Position(0, i)) &&
              white_castle.count(start))) &&
            get_piece(Position(1, i)) == nullptr &&
            get_piece(Position(2, i)) == nullptr &&
            get_piece(Position(3, i)) == nullptr &&
            get_piece_enum(get_piece(Position(start))) == ChessPiece::King &&
            get_piece_enum(get_piece(Position(0, i))) == ChessPiece::Rook)
        {
            // Same spiel as king, but this time queen-side
            remove_piece(Position(4, i));
            remove_piece(Position(0, i));
            add_piece<King>(Position(1, i), color);
            add_piece<Rook>(Position(2, i), color);
            if (black_castle.count(start))
                black_castle.erase(black_castle.find(start));
            if (white_castle.count(start))
                white_castle.erase(white_castle.find(start));
            en_passant.erase(next_player);
            next_player = next_player == ChessColor::White ? ChessColor::Black : ChessColor::White;
            return true;
        }
    }
    return false;
}

bool ChessBoard::handle_en_passant(const Position &start, const Position &goal)
{
    if (get_piece_enum(get_piece(start)) == ChessPiece::Pawn)
    {
        // Check if white attacked black
        if (get_piece(start)->get_player() == ChessColor::Black &&
            start.y == 3 &&
            goal.y == 2 &&
            en_passant[ChessColor::White] == goal &&
            abs(goal.x - start.x) == 1)
        {
            // Remove enemy piece
            remove_piece(Position(goal.x, 3));
            // Clear en passant
            en_passant.erase(next_player);
            // Switch turns
            next_player = next_player == ChessColor::White ? ChessColor::Black : ChessColor::White;
            return true;
        }
        else
            // Check if black attacked white
            if (get_piece(start)->get_player() == ChessColor::White &&
                start.y == 4 &&
                goal.y == 5 &&
                en_passant[ChessColor::Black] == goal &&
                abs(goal.x - start.x) == 1)
            {
                // Remove enemy piece
                remove_piece(Position(goal.x, 4));
                // Clear en passant
                en_passant.erase(next_player);
                // Switch turns
                next_player = next_player == ChessColor::White ? ChessColor::Black : ChessColor::White;
                return true;
            }
    }
    return false;
}

void ChessBoard::handle_promotion(const Position &goal)
{
    // Get current player's color
    int current_color = get_piece(goal)->get_player();

    // Check if you have advanced a pawn to your respective highest rank
    if (((goal.y == 7 && current_color == ChessColor::White) || (goal.y == 0 && current_color == ChessColor::Black)) && get_piece_enum(get_piece(goal)) == ChessPiece::Pawn)
    {
        // Prompt the user what they want to promote to
        ChessPiece promoted_piece = this->promotion_prompt();
        remove_piece(goal);
        switch (promoted_piece)
        {
        case ChessPiece::Rook:
            add_piece<Rook>(goal, current_color);
            break;
        case ChessPiece::Bishop:
            add_piece<Bishop>(goal, current_color);
            break;
        case ChessPiece::Queen:
            add_piece<Queen>(goal, current_color);
            break;
        case ChessPiece::Knight:
            add_piece<Knight>(goal, current_color);
            break;
        default:
            throw;
        }
    }
}
