#include "position.h"

// Adding operators to the class, such that we can use them in sets

Position &Position::operator+=(const Position &rhs)
{
    x += rhs.x;
    y += rhs.y;
    return *this;
}

Position &Position::operator-=(const Position &rhs)
{
    x -= rhs.x;
    y -= rhs.y;
    return *this;
}

bool operator==(const Position &lhs, const Position &rhs) { return lhs.x == rhs.x && lhs.y == rhs.y; }
bool operator!=(const Position &lhs, const Position &rhs) { return !operator==(lhs, rhs); }

Position operator+(Position lhs, const Position &rhs)
{
    lhs += rhs;
    return lhs;
}

Position operator-(Position lhs, const Position &rhs)
{
    lhs -= rhs;
    return lhs;
}
