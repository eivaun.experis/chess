#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <utility>

#include "position.h"

struct Move
{
    Position start;
    Position goal;
};

// Parses a move, e.g. e2f3, into a start and goal position
bool parse_move(const std::string &command, Move& move);
