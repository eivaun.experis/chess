#pragma once

#include <vector>
#include <unordered_set>
#include <memory>

#include "position.h"
#include "piece.h"

class Board
{
public:
    Board() = default;
    Board(int width, int height);
    virtual ~Board(){};

    int get_width() const { return width; }
    int get_height() const { return height; }

    const std::vector<std::vector<std::unique_ptr<Piece>>> &get_squares() const;

    // Constructs and attempts to place a piece in the given position
    template <class T, typename... Args>
    bool add_piece(const Position &position, Args &&...args);

    // Attempts to place a piece in the given position
    virtual bool add_piece(const Position &position, std::unique_ptr<Piece> piece);

    // Removes the piece in the given position
    virtual bool remove_piece(const Position &position);

    // Attempts to move a piece from start to goal
    virtual bool move_piece(const Position &start, const Position &goal);

    // Get a pointer to the piece in the given position.
    Piece *get_piece(const Position &position) const;

    // Get the player owning the piece in the given position.
    int get_player(const Position &position) const;

    // Returns true if the piece in the start position can move to the goal position
    bool check_if_legal_move(const Position &start, const Position &goal) const;

    // Returns all the positions the piece in the start position can move to
    virtual std::unordered_set<Position> get_legal_moves(const Position &start) const;

protected:
    // Get a reference to the piece pointer. Not range checked
    std::unique_ptr<Piece> &get_ptr(const Position &position);

private:
    int width, height;
    std::vector<std::vector<std::unique_ptr<Piece>>> board;
};

template <class T, typename... Args>
bool Board::add_piece(const Position &position, Args &&...args)
{
    auto ptr = std::make_unique<T>(std::forward<Args>(args)...);
    return add_piece(position, std::move(ptr));
}
