#pragma once

#include "board.h"
#include "chess.h"
#include <unordered_set>
#include <map>

class ChessBoard : public Board
{
    ChessColor next_player = ChessColor::White;
    std::unordered_set<Position> white_castle = {Position(0, 7), Position(7, 7), Position(4, 7)};
    std::unordered_set<Position> black_castle = {Position(0, 0), Position(7, 0), Position(4, 0)};
    std::map<int, Position> en_passant;
    std::function<ChessPiece()> promotion_prompt;

public:
    ChessBoard();

    // Sets up a regular 8x8 chess board with pieces in place
    void setup_board();

    // Returns all the positions the piece in the start position can move to
    std::unordered_set<Position> get_legal_moves(const Position &start) const override;

    // Moves a piece from start to goal, assuming it is a legal move and your turn
    bool move_piece(const Position &start, const Position &goal) override;

    // Used to retrieve and set whose turn it is
    ChessColor get_next_player() const { return next_player; }
    void set_next_player(ChessColor player) { next_player = player; }

    // Returns a set of pieces which haven't moved. Used when checking for castling
    std::unordered_set<Position> &get_white() { return white_castle; }
    std::unordered_set<Position> &get_black() { return black_castle; }
    const std::unordered_set<Position> &get_white() const { return white_castle; }
    const std::unordered_set<Position> &get_black() const { return black_castle; }

    // Returns a set of positions where you are able to perform en passant
    std::map<int, Position> &get_en_passant() { return en_passant; }
    const std::map<int, Position> &get_en_passant() const { return en_passant; }

    // Checks if any enemy piece can attack the piece in pos
    bool can_any_piece_attack(const Position &pos, ChessColor defending_player, bool check_if_check) const;

    // Checks for "check"
    bool is_check(ChessColor defending_player) const;
    bool move_is_check(const Position &start, const Position &goal, ChessColor defending_player) const;

    // Checks for "checkmate" and "stalemate"
    bool is_checkmate(ChessColor defending_player) const;
    bool is_stalemate() const;

    // Sets how to prompt the user for promotion of a pawn. Mostly useful for unit testing
    void set_promotion_prompt(std::function<ChessPiece()> promotion_prompt);

private:
    bool handle_castling(const Position &start, const Position &goal);
    bool handle_en_passant(const Position &start, const Position &goal);
    void handle_promotion(const Position &goal);
};
