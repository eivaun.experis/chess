#include "printer.h"
#include "chess_pieces.h"
#include <iostream>
#include <iomanip>
#include "chess.h"
#include "position.h"
#include "input.h"

void Printer::set_preferences()
{
    // Asks the user whether they want to use unicode
    Input::out() << "Unicode:     ♟ ♜ ♞ ♝ ♛ ♚ ♙ ♖ ♘ ♗ ♕ ♔ \n";
    Input::out() << "Not Unicode: wP wR wN wB wQ wK bP bR bN bB bQ bK \n";
    unicode = Input::read_bool("Use unicode characters");

    if (unicode)
    {
        // Asks them whether to use extra spaces or not
        Input::out() << "Space:    |♟ ||♜ ||♞ ||♝ ||♛ ||♚ |\n";
        Input::out() << "No space: |♟| |♜| |♞| |♝| |♛| |♚|\n";
        space = Input::read_bool("Use extra space");
    }
}

void Printer::print_board(const Board &board)
{
    // Print top row
    if (!unicode || space)
        Input::out() << "╔══╤══╤══╤══╤══╤══╤══╤══╗" << std::endl;
    else
        Input::out() << "╔═╤═╤═╤═╤═╤═╤═╤═╗" << std::endl;

    // Print "content"
    for (int y = 7; y >= 0; y--)
    {
        // Every row print a wall, all the pieces with divivers, then another wall
        // Need to translate pieces to their character counterparts
        Input::out() << "║";
        for (int x = 0; x < 8; x++)
        {
            Piece *temp = board.get_piece(Position(x, y));
            if (temp == nullptr)
            {
                if (!unicode)
                    Input::out() << " ";
                Input::out() << " ";
            }
            else
            {
                // Translation time
                ChessPiece piece_type = get_piece_enum(temp);
                ChessColor piece_color = (ChessColor)temp->get_player();
                switch (piece_type)
                {
                case ChessPiece::Pawn:
                    if (piece_color == ChessColor::Black)
                        if (unicode)
                            Input::out() << "♙";
                        else
                            Input::out() << "bP";
                    else if (unicode)
                        Input::out() << "♟";
                    else
                        Input::out() << "wP";
                    break;
                case ChessPiece::Rook:
                    if (piece_color == ChessColor::Black)
                        if (unicode)
                            Input::out() << "♖";
                        else
                            Input::out() << "bR";
                    else if (unicode)
                        Input::out() << "♜";
                    else
                        Input::out() << "wR";
                    break;
                case ChessPiece::Knight:
                    if (piece_color == ChessColor::Black)
                        if (unicode)
                            Input::out() << "♘";
                        else
                            Input::out() << "bN";
                    else if (unicode)
                        Input::out() << "♞";
                    else
                        Input::out() << "wN";
                    break;
                case ChessPiece::Bishop:
                    if (piece_color == ChessColor::Black)
                        if (unicode)
                            Input::out() << "♗";
                        else
                            Input::out() << "bB";
                    else if (unicode)
                        Input::out() << "♝";
                    else
                        Input::out() << "wB";
                    break;
                case ChessPiece::Queen:
                    if (piece_color == ChessColor::Black)
                        if (unicode)
                            Input::out() << "♕";
                        else
                            Input::out() << "bQ";
                    else if (unicode)
                        Input::out() << "♛";
                    else
                        Input::out() << "wQ";
                    break;
                case ChessPiece::King:
                    if (piece_color == ChessColor::Black)
                        if (unicode)
                            Input::out() << "♔";
                        else
                            Input::out() << "bK";
                    else if (unicode)
                        Input::out() << "♚";
                    else
                        Input::out() << "wK";
                    break;

                default:
                    break;
                }
            }

            if (unicode && space)
                Input::out() << " ";

            if (x < 7)
                Input::out() << "│";
            else
                Input::out() << "║";
        }

        Input::out() << " " << (y + 1) << std::endl;

        // Print dividers or bottom rows depending on counter
        if (y > 0)
            if (!unicode || space)
                Input::out() << "╟──┼──┼──┼──┼──┼──┼──┼──╢\n";
            else
                Input::out() << "╟─┼─┼─┼─┼─┼─┼─┼─╢\n";
        else if (!unicode || space)
            Input::out() << "╚══╧══╧══╧══╧══╧══╧══╧══╝" << std::endl;
        else
            Input::out() << "╚═╧═╧═╧═╧═╧═╧═╧═╝" << std::endl;
    }

    for (int x = 0; x < 8; x++)
    {
        Input::out() << " " << char('a' + x) << ((space || !unicode) ? " " : "");
    }
    Input::out() << std::endl;
};

void Printer::print_save_list(const std::vector<SaveInfo> &saves)
{
    // Print out the list of available saves
    if (saves.size() == 0)
    {
        Input::out() << "No save files\n";
        return;
    }

    int w[] = {6, 26};

    Input::out() << std::setw(w[0]) << "Slot"
                 << std::setw(w[1]) << "Date"
                 << "Name" << std::endl;
    for (const auto &save : saves)
    {
        Input::out() << std::setw(w[0]) << save.slot;

        std::stringstream ss;
        ss << std::ctime(&save.time_saved);
        auto str = ss.str();
        str.pop_back();
        Input::out() << std::setw(w[1]) << str;

        Input::out() << save.name << std::endl;
    }
}
