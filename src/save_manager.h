#pragma once

#include <vector>
#include <string>
#include <chrono>

#include "chess_board.h"
#include "recorder.h"

struct SaveInfo
{
    int slot = -1;              // Slot number
    std::string name{};         // Name of save slot
    std::time_t time_saved{};   // Time it was created
	Recorder recorder;			// A record of the performed moves

    template <class B>
    void serialize(B &buf) const
    {
        buf << slot << name << time_saved << recorder;
    }

    template <class B>
    void parse(B &buf)
    {
        buf >> slot >> name >> time_saved >> recorder;
    }
};

class SaveManager
{
    inline static std::string save_location = "./saves/";
    inline static std::string save_name = "save";
    inline static std::string save_extension = ".bin";

    inline static std::string get_save_path(int i);

public:
    // Save board to file
    static bool save_state(const ChessBoard &board, SaveInfo &info);

    // Load board from file
    static bool load_state(ChessBoard &board, SaveInfo &info, int slot);

	// Load the data from a save file without the board
	static bool load_info(SaveInfo &info, int slot);

    // Get list of existing saves
    static std::vector<SaveInfo> get_existing_saves();
};
