#pragma once

#include "chess_pieces.h"

enum ChessColor
{
    None,
    White,
    Black
};

enum class ChessPiece
{
    Invalid = -1,
    Pawn,
    Rook,
    Knight,
    Bishop,
    Queen,
    King
};

inline ChessPiece get_piece_enum(Piece *piece)
{
    if (dynamic_cast<Pawn *>(piece))
        return ChessPiece::Pawn;
    if (dynamic_cast<Rook *>(piece))
        return ChessPiece::Rook;
    if (dynamic_cast<Knight *>(piece))
        return ChessPiece::Knight;
    if (dynamic_cast<Bishop *>(piece))
        return ChessPiece::Bishop;
    if (dynamic_cast<Queen *>(piece))
        return ChessPiece::Queen;
    if (dynamic_cast<King *>(piece))
        return ChessPiece::King;

    return ChessPiece::Invalid;
}

inline std::unique_ptr<Piece> create_piece_from_enum(ChessPiece piece, int player)
{
    switch (piece)
    {
    case ChessPiece::Pawn:
        return std::make_unique<Pawn>(player);
    case ChessPiece::Rook:
        return std::make_unique<Rook>(player);
    case ChessPiece::Knight:
        return std::make_unique<Knight>(player);
    case ChessPiece::Bishop:
        return std::make_unique<Bishop>(player);
    case ChessPiece::Queen:
        return std::make_unique<Queen>(player);
    case ChessPiece::King:
        return std::make_unique<King>(player);
    default:
        throw;
    }
}
