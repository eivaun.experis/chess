#include "parse_move.h"

bool parse_move(const std::string &command, Move &move)
{
	if (command.size() < 4 || command.size() > 5)
		return false;

	move.start.x = command[0] - 'a';
	move.start.y = command[1] - '1';

	int pos = command[2] == ' ' ? 3 : 2;
	move.goal.x = command[pos] - 'a';
	move.goal.y = command[pos + 1] - '1';

	if (move.start.x > 7 || move.start.x < 0 || move.start.y > 7 || move.start.y < 0 || move.goal.x > 7 || move.goal.x < 0 || move.goal.y > 7 || move.goal.y < 0)
	{
		return false;
	}
	return true;
}