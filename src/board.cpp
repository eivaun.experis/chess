#include "board.h"

Board::Board(int width, int height) : width(width), height(height)
{
    board.resize(height);
    for (auto &row : board)
    {
        row.resize(width);
    }
}

const std::vector<std::vector<std::unique_ptr<Piece>>> &Board::get_squares() const { return board; }

bool Board::add_piece(const Position &position, std::unique_ptr<Piece> piece)
{
    if (get_piece(position) != nullptr)
        return false;

    get_ptr(position) = std::move(piece);
    return true;
}

bool Board::remove_piece(const Position &position)
{
    if (get_piece(position) == nullptr)
        return false;

    get_ptr(position).reset();
    return true;
}

bool Board::move_piece(const Position &start, const Position &goal)
{
    if (!check_if_legal_move(start, goal))
        return false;

    std::swap(get_ptr(start), get_ptr(goal));
    return true;
}

Piece *Board::get_piece(const Position &position) const
{
    if (position.x < 0 || position.x >= width || position.y < 0 || position.y >= height)
        return nullptr;
    return board[position.y][position.x].get();
}

int Board::get_player(const Position &position) const
{
    auto ptr = get_piece(position);
    if (ptr == nullptr)
        return -1;
    return ptr->get_player();
}

bool Board::check_if_legal_move(const Position &start, const Position &goal) const
{
    auto legal_moves = get_legal_moves(start);
    return legal_moves.count(goal);
}

std::unordered_set<Position> Board::get_legal_moves(const Position &start) const
{
    auto ptr = get_piece(start);
    if (ptr == nullptr)
        return {};
    return ptr->get_all_legal_moves(*this, start);
}

std::unique_ptr<Piece> &Board::get_ptr(const Position &position)
{
    return board[position.y][position.x];
}