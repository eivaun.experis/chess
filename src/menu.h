#pragma once

#include <string>
#include <functional>
#include <vector>

// A menu item
struct MenuItem
{
    // The command to select the item
    std::string command;
    // The name of the item
    std::string name;
    // The function executed if the item is selected
    std::function<void()> func;
};

// A menu is just a collection of items
using Menu = std::vector<MenuItem>;

// Display a menu with the given items until running is set to false
void do_menu(const std::string &title, Menu menu, bool &running);

// Display a menu with the given items until the 'back' command is selected
void do_menu(const std::string &title, Menu menu);
