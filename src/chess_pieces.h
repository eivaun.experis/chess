#pragma once

#include "chess_pieces/pawn.h"
#include "chess_pieces/rook.h"
#include "chess_pieces/knight.h"
#include "chess_pieces/bishop.h"
#include "chess_pieces/queen.h"
#include "chess_pieces/king.h"
