#pragma once

#include <vector>
#include <unordered_set>

#include "hps/hps.h"
#include "chess_board.h"

class ChessBoardSerializer
{
    const ChessBoard *board;

    struct Data
    {
        Position pos;
        std::unique_ptr<Piece> piece;
    };

    std::vector<Data> data;
    int next_player;
    std::unordered_set<Position> white_castle;
    std::unordered_set<Position> black_castle;
    std::map<int, Position> en_passant;

public:
    ChessBoardSerializer() = default;

    // Set the board that gets serialized
    void set_serialization_target(const ChessBoard *board);

    // Loads the deserialized data in to the given board
    void load_serialization_data(ChessBoard &board);

    // Save
    template <class B>
    void serialize(B &buf) const
    {
        buf << board->get_width() << board->get_height() << int(board->get_next_player());

        // Get the number of pieces
        int num_pieces = 0;
        const auto &board_squares = board->get_squares();
        for (const auto &row : board_squares)
        {
            for (const auto &square : row)
                if (square != nullptr)
                    num_pieces++;
        }
        buf << num_pieces;

        // Save the pieces
        for (int y = 0; y < board->get_height(); y++)
        {
            for (int x = 0; x < board->get_width(); x++)
            {
                auto piece = board->get_piece({x, y});
                if (piece == nullptr)
                    continue;

                buf << x << y << piece->get_player() << int(get_piece_enum(piece));
            }
        }

        buf << board->get_white() << board->get_black() << board->get_en_passant();
    }

    // Load
    template <class B>
    void parse(B &buf)
    {
        int width, height;
        buf >> width >> height >> next_player;

        int num_pieces = 0;
        buf >> num_pieces;

        data.resize(num_pieces);

        for (int i = 0; i < num_pieces; i++)
        {
            int x, y, player, piece_enum;
            buf >> x >> y >> player >> piece_enum;

            data[i].pos = {x, y};
            data[i].piece = create_piece_from_enum(ChessPiece(piece_enum), player);
        }
        buf >> white_castle >> black_castle >> en_passant;
    }
};
