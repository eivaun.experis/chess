#pragma once

#include "util/hash_combiner.h"

struct Position
{
    int x, y;

    Position() : x(0), y(0) {}
    Position(int x, int y) : x(x), y(y) {}

    Position &operator+=(const Position &rhs);
    Position &operator-=(const Position &rhs);

    template<class Buffer>
    void serialize(Buffer& buffer) const {
        buffer << x << y;
    }

    template<class Buffer>
    void parse(Buffer& buffer) {
        buffer >> x >> y;
    }
};

bool operator==(const Position &lhs, const Position &rhs);
bool operator!=(const Position &lhs, const Position &rhs);

Position operator+(Position lhs, const Position &rhs);
Position operator-(Position lhs, const Position &rhs);

// Hash function to allow use in std::unordered_set
namespace std
{
    template <>
    struct hash<Position>
    {
        inline std::size_t operator()(const Position &v) const
        {
            std::size_t seed = 0;
            hash_combine(seed, v.x);
            hash_combine(seed, v.y);
            return seed;
        }
    };
}
