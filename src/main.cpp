#include "program.h"

#include "recorder.h"
#include "board.h"
#include "position.h"
#include "chess_board.h"
#include "chess.h"
#include "chess_pieces.h"

int main()
{
    Program::run();
    return 0;
}
