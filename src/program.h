#include "menu.h"
#include "input.h"
#include "chess_board.h"
#include "printer.h"
#include "save_manager.h"
#include "parse_move.h"
#include "chess.h"

class Program
{

public:
	static void run()
	{
		// Ask the user for display preferences
		Printer::set_preferences();

		bool running = true;
		do_menu("Main Menu",
				{
					{"new", "Start a new game", [&]()
					 {
						 ChessBoard b;
						 b.setup_board();
						 auto saves = SaveManager::get_existing_saves();
						 Printer::print_save_list(saves);
						 SaveInfo save;
						 save.slot = Input::read_int("Select save slot", 1, 10);
						 save.name = Input::read_string("Save name");
						 bool saved = SaveManager::save_state(b, save);
						 if (saved)
							 play(b, save);
						 else
							 Input::out() << "Failed to create new game\n";
					 }},
					{"load", "Load a previous game", [&]()
					 {
						 auto saves = SaveManager::get_existing_saves();
						 Printer::print_save_list(saves);
						 int save_slot = Input::read_int("Select save slot", 1, 10);
						 ChessBoard b;
						 SaveInfo save;
						 bool loaded = SaveManager::load_state(b, save, save_slot);
						 if (loaded)
							 play(b, save);
						 else
							 Input::out() << "Failed to load game\n";
					 }},
					{"replay", "Watch a replay of previous game", [&]()
					 {
						 auto saves = SaveManager::get_existing_saves();
						 Printer::print_save_list(saves);
						 int save_slot = Input::read_int("Select save slot", 1, 10);
						 ChessBoard b;
						 SaveInfo save;
						 b.setup_board();
						 bool loaded = SaveManager::load_info(save, save_slot);
						 if (loaded)
							 replay(b, save);
						 else
							 Input::out() << "Failed to replay game\n";
					 }},
					{"exit", "Exit the game", [&]()
					 {
						 running = false;
					 }},
				},
				running);
	}

private:
	// Play the selected game
	static void play(ChessBoard &board, SaveInfo &save)
	{
		// The promt shown when promoting a pawn
		board.set_promotion_prompt([&]()
		{
			ChessPiece piece = ChessPiece::Invalid;

			bool running = true;
			do_menu("What would you like to promote your pawn to?",
			{
				{"Q", "Queen", [&]()
					{
						piece = ChessPiece::Queen;
						running = false;
					}},
				{"K", "Knight", [&]()
					{
						piece = ChessPiece::Knight;
						running = false;
					}},
				{"B", "Bishop", [&]()
					{
						piece = ChessPiece::Bishop;
						running = false;
					}},
				{"R", "Rook", [&]()
					{
						piece = ChessPiece::Rook;
						running = false;
					}},
			}, running);

			save.recorder.record_promotion(piece);
			return piece; 
		});

		bool playing = true;
		bool game_over = false;
		int winner = ChessColor::None;
		while (playing)
		{
			Printer::print_board(board);

			if (board.is_stalemate())
			{
				Input::out() << "Stalemate!\n";
				winner = ChessColor::None;
				game_over = true;
				break;
			}
			else if (board.is_checkmate(board.get_next_player()))
			{
				Input::out() << "Checkmate!\n";
				winner = board.get_next_player() == ChessColor::White ? ChessColor::Black : ChessColor::White;
				game_over = true;
				break;
			}
			else if (board.is_check(board.get_next_player()))
			{
				Input::out() << "Check!\n";
			}

			// Retry until valid input
			while (true)
			{
				Input::out() << (board.get_next_player() == ChessColor::White ? "White" : "Black") << "'s turn\n";
				auto input = Input::read_string("Move");
				if (input == "exit")
				{
					playing = false;
					break;
				}
				else if (input == "concede")
				{
					save.recorder.record_move(input);
					winner = board.get_next_player() == ChessColor::White ? ChessColor::Black : ChessColor::White;
					playing = false;
					game_over = true;
					break;
				}

				Move move;
				if (!parse_move(input, move))
				{
					Input::out() << "Invalid command\n";
					continue;
				}

				if (board.move_piece(move.start, move.goal))
				{
					save.recorder.record_move(input);
					break;
				}
				else
				{
					Input::out() << "Invalid move\n";
				}
			}

			SaveManager::save_state(board, save);
		}

		if (game_over)
		{
			Printer::print_board(board);
			if (winner == ChessColor::None)
			{
				Input::out() << "Game ended in stalemate\n";
			}
			else
			{
				Input::out() << (winner == ChessColor::White ? "White" : "Black") << " won!\n";
			}
		}
	}

	// Replay the selected game
	static void replay(ChessBoard &board, SaveInfo &save)
	{
		board.set_promotion_prompt([&]() { return save.recorder.next_promotion(); });

		Printer::print_board(board);
		bool replaying = true;
		bool game_over = false;
		int winner = ChessColor::None;
		while (replaying)
		{

			if (board.is_stalemate())
			{
				Input::out() << "Stalemate!\n";
				winner = ChessColor::None;
				game_over = true;
				break;
			}
			else if (board.is_checkmate(board.get_next_player()))
			{
				Input::out() << "Checkmate!\n";
				winner = board.get_next_player() == ChessColor::White ? ChessColor::Black : ChessColor::White;
				game_over = true;
				break;
			}
			else if (board.is_check(board.get_next_player()))
			{
				Input::out() << "Check!\n";
			}

			while (true)
			{

				auto input = Input::read_string("\"exit\" to quit or enter to continue", true);

				if (input == "exit")
				{
					replaying = false;
					break;
				}

				auto next_move = save.recorder.replay();

				if (next_move == "")
				{
					Input::out() << "No more moves. \n";
					break;
				}
				else if (next_move == "concede")
				{
					save.recorder.record_move(input);
					winner = board.get_next_player() == ChessColor::White ? ChessColor::Black : ChessColor::White;
					replaying = false;
					game_over = true;
					break;
				}

				Move move;
				parse_move(next_move, move);
				board.move_piece(move.start, move.goal);

				Printer::print_board(board);
				Input::out() << (board.get_next_player() == ChessColor::Black ? "White" : "Black") << " moves " << next_move << "\n";
			}
		}

		if (game_over)
		{
			Printer::print_board(board);
			if (winner == ChessColor::None)
			{
				Input::out() << "Game ended in stalemate\n";
			}
			else
			{
				Input::out() << (winner == ChessColor::White ? "White" : "Black") << " won!\n";
			}
		}
	}
};
