#include "menu.h"

#include <iomanip>
#include "input.h"

// Display a menu with the given items until running is set to false
void do_menu(const std::string &title, Menu menu, bool &running)
{
    std::string unknown = "";
    while (running)
    {
        Input::out() << "\n===== " << title << " =====" << std::endl;
        for (const auto &item : menu)
        {
            Input::out() << std::left << std::setw(10) << item.command << item.name << std::endl;
        }

        Input::out() << std::endl;

        std::string input = Input::read_string("Enter command");
        std::transform(input.begin(), input.end(), input.begin(), ::tolower);

        Input::out() << std::endl;

        auto it = std::find_if(menu.begin(), menu.end(), [&](const MenuItem &item)
        { 
            std::string command_lowercase = item.command;
            std::transform(command_lowercase.begin(), command_lowercase.end(), command_lowercase.begin(), ::tolower);
            return command_lowercase == input; 
        });

        if (it == menu.end())
        {
            Input::out() << "Unknown command: " << input << std::endl;
        }
        else
        {
            (*it).func();
        }
    }
}

// Display a menu with the given items until the 'back' command is selected
void do_menu(const std::string &title, Menu menu)
{
    bool running = true;
    menu.push_back({"back", "Back to the previous menu", [&]()
                    {
                        running = false;
                    }});
    do_menu(title, menu, running);
}
