#pragma once

#include <vector>
#include <string>

#include "chess.h"

class Recorder
{
protected:
	std::vector<std::string> moves;
	std::vector<std::string>::iterator move_it;

	std::vector<int> promotions;
	std::vector<int>::iterator promotion_it;

public:
	Recorder() = default;

	// Record a move
	void record_move(const std::string &move)
	{
		moves.push_back(move);
	}

	// Record a promotion
	void record_promotion(ChessPiece piece)
	{
		promotions.push_back((int)piece);
	}

	// Start on the beginning of the game and returns each move
	std::string replay()
	{
		if (move_it == moves.end())
			return "";
		return *(move_it++);
	}

	// Return the next recorded promotion
	ChessPiece next_promotion()
	{
		if (promotion_it == promotions.end())
			return ChessPiece::Invalid;
		return ChessPiece(*(promotion_it++));
	}

	template <class Serializer>
	void serialize(Serializer &s) const
	{
		s << moves << promotions;
	}

	template <class Serializer>
	void parse(Serializer &s)
	{
		s >> moves >> promotions;

		move_it = moves.begin();
		promotion_it = promotions.begin();
	}
};