#pragma once

#include "../piece.h"

class King : public Piece
{
public:
	King() = default;
	King(int player);
	std::unordered_set<Position> get_all_legal_moves(const Board &board, const Position &position) const override;
};
