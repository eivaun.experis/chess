#include "king.h"
#include "../board.h"
#include "../chess.h"
#include "chess_board.h"

King::King(int player) : Piece(player){};

std::unordered_set<Position> King::get_all_legal_moves(const Board &board, const Position &position) const
{
	std::unordered_set<Position> legal_positions;

	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y <= 1; y++)
		{
			Position pos = position + Position(x, y);
			if ((x == 0 && y == 0) || pos.x < 0 || pos.y < 0 || pos.x > 7 || pos.y > 7)
				continue;

			if (board.get_piece(pos) == nullptr || board.get_player(pos) != this->get_player())
			{
				legal_positions.insert(pos);
			}
		}
	}

	return legal_positions;
}
