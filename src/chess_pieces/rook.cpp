#include "rook.h"
#include "../board.h"
#include "../position.h"

Rook::Rook(int player) : Piece(player){};

std::unordered_set<Position> Rook::get_all_legal_moves(const Board &board, const Position &position) const
{
    std::unordered_set<Position> legal_positions;

    int player;
    // Check towards right
    for (int i = position.x + 1; i < 8; i++)
    {
        Position pos = Position(i, position.y);
        // Add positions until we hit another piece, then break
        player = board.get_player(pos);
        if (player != this->get_player())
        {
            // In case that it is an enemy piece, add before breaking
            legal_positions.insert(pos);
            if (board.get_piece(pos) != nullptr)
            {
                break;
            }
        }
        else
        {
            break;
        }
    }

    // Check towards left
    for (int i = position.x - 1; i >= 0; i--)
    {
        Position pos = Position(i, position.y);
        // Add positions until we hit another piece, then break
        player = board.get_player(pos);
        if (player != this->get_player())
        {
            // In case that it is an enemy piece, add before breaking
            legal_positions.insert(pos);
            if (board.get_piece(pos) != nullptr)
            {
                break;
            }
        }
        else
        {
            break;
        }
    }

    // Check upwards
    for (int i = position.y - 1; i >= 0; i--)
    {
        Position pos = Position(position.x, i);
        // Add positions until we hit another piece, then break
        player = board.get_player(pos);
        if (player != this->get_player())
        {
            // In case that it is an enemy piece, add before breaking
            legal_positions.insert(pos);
            if (board.get_piece(pos) != nullptr)
            {
                break;
            }
        }
        else
        {
            break;
        }
    }

    // Check downwards
    for (int i = position.y + 1; i < 8; i++)
    {
        Position pos = Position(position.x, i);
        // Add positions until we hit another piece, then break
        player = board.get_player(pos);
        if (player != this->get_player())
        {
            // In case that it is an enemy piece, add before breaking
            legal_positions.insert(pos);
            if (board.get_piece(pos) != nullptr)
            {
                break;
            }
        }
        else
        {
            break;
        }
    }

    return legal_positions;
};
