#include "pawn.h"
#include "../board.h"
#include "../chess.h"

Pawn::Pawn(int player) : Piece(player){};

std::unordered_set<Position> Pawn::get_all_legal_moves(const Board &board, const Position &position) const
{
	std::unordered_set<Position> legal_position;

	int dir = 1;
	int start_row = 1;
	if (get_player() == ChessColor::Black)
	{
		dir = -1;
		start_row = 6;
	}

	// check forward one move and two
	if (board.get_piece(position + Position(0, dir)) == nullptr)
	{
		if (position.y == start_row)
		{
			if (board.get_piece(Position(position.x, position.y + (dir * 2))) == nullptr)
			{
				legal_position.insert(Position(position.x, position.y + (2 * dir)));
			}
		}
		legal_position.insert(Position(position.x, position.y + dir));
	}

	// Tackle sideways right
	if (board.get_piece(position + Position(1, dir)) != nullptr && board.get_player(position + Position(1, dir)) != this->get_player())
	{
		legal_position.insert(Position(position.x + 1, position.y + dir));
	}

	// tackle sideways left
	if (board.get_piece(position + Position(-1, dir)) != nullptr && board.get_player(position + Position(1, dir)) != this->get_player())
	{
		legal_position.insert(Position(position.x - 1, position.y + dir));
	}

	return legal_position;
}