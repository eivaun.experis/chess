#include "knight.h"
#include "../board.h"
#include "../position.h"

Knight::Knight(int player) : Piece(player){};

std::unordered_set<Position> Knight::get_all_legal_moves(const Board &board, const Position &position) const
{
    std::unordered_set<Position> legal_positions;

    for (int x : {-2, -1, 1, 2})
    {
        for (int y : {-2, -1, 1, 2})
        {
            if (abs(x) == abs(y))
                continue;

            Position pos(position.x + x, position.y + y);
            if (pos.x < 0 || pos.x > 7 || pos.y < 0 || pos.y > 7)
                continue;

            if (board.get_player(pos) != this->get_player())
                legal_positions.insert(pos);
        }
    }

    return legal_positions;
};
