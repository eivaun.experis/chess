#pragma once

#include "../piece.h"

class Bishop : public Piece
{
public:
    Bishop() = default;
    Bishop(int player);
    std::unordered_set<Position> get_all_legal_moves(const Board &board, const Position &position) const override;
};
