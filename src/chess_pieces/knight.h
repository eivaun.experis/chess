#pragma once

#include "../piece.h"

class Knight : public Piece
{
public:
    Knight() = default;
    Knight(int player);
    std::unordered_set<Position> get_all_legal_moves(const Board &board, const Position &position) const override;
};
