#include "bishop.h"
#include "../board.h"
#include "../position.h"

Bishop::Bishop(int player) : Piece(player){};

std::unordered_set<Position> Bishop::get_all_legal_moves(const Board &board, const Position &position) const
{
    std::unordered_set<Position> legal_positions;

    // Check towards top right
    for (int x = position.x + 1, y = position.y + 1; x < 8 && y < 8; x++, y++)
    {
        Position pos = Position(x, y);
        Piece *piece = board.get_piece(pos);
        if (piece == nullptr)
        {
            legal_positions.insert(pos);
            continue;
        }

        if (piece->get_player() != board.get_piece(position)->get_player())
            legal_positions.insert(pos);

        break;
    }

    // Check towards top left
    for (int x = position.x - 1, y = position.y + 1; x >= 0 && y < 8; x--, y++)
    {
        Position pos = Position(x, y);
        Piece *piece = board.get_piece(pos);
        if (piece == nullptr)
        {
            legal_positions.insert(pos);
            continue;
        }

        if (piece->get_player() != board.get_piece(position)->get_player())
            legal_positions.insert(pos);

        break;
    }

    // Check towards bottom right
    for (int x = position.x + 1, y = position.y - 1; x < 8 && y >= 0; x++, y--)
    {
        Position pos = Position(x, y);
        Piece *piece = board.get_piece(pos);
        if (piece == nullptr)
        {
            legal_positions.insert(pos);
            continue;
        }

        if (piece->get_player() != board.get_piece(position)->get_player())
            legal_positions.insert(pos);

        break;
    }

    // Check towards bottom left
    for (int x = position.x - 1, y = position.y - 1; x >= 0 && y >= 0; x--, y--)
    {
        Position pos = Position(x, y);
        Piece *piece = board.get_piece(pos);
        if (piece == nullptr)
        {
            legal_positions.insert(pos);
            continue;
        }

        if (piece->get_player() != board.get_piece(position)->get_player())
            legal_positions.insert(pos);

        break;
    }

    return legal_positions;
};
