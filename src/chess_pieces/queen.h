#pragma once

#include "../piece.h"

class Queen : public Piece
{
public:
    Queen() = default;
    Queen(int player);
    std::unordered_set<Position> get_all_legal_moves(const Board &board, const Position &position) const override;
};
