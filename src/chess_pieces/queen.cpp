#include "queen.h"

#include <algorithm>

#include "../board.h"
#include "../position.h"
#include "rook.h"
#include "bishop.h"

Queen::Queen(int player) : Piece(player){};

std::unordered_set<Position> Queen::get_all_legal_moves(const Board &board, const Position &position) const
{
    std::unordered_set<Position> legal_positions;

    // As the queens movement basically consists of the rook's movement and the bishop's movement,
    // we just simulate those two pieces and add their resulting legal moves together

    Rook rook(get_player());
    std::unordered_set<Position> rook_movement = rook.get_all_legal_moves(board, position);

    Bishop bishop(get_player());
    std::unordered_set<Position> bishop_movement = bishop.get_all_legal_moves(board, position);

    for (auto &it : rook_movement)
        legal_positions.insert(it);

    for (auto &it : bishop_movement)
        legal_positions.insert(it);

    return legal_positions;
};
