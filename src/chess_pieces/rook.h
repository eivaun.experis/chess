#pragma once

#include "../piece.h"

class Rook : public Piece
{
public:
    Rook() = default;
    Rook(int player);
    std::unordered_set<Position> get_all_legal_moves(const Board &board, const Position &position) const override;
};
