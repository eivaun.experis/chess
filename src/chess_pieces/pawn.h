#pragma once

#include "../piece.h"

class Pawn : public Piece
{
public:
	Pawn() = default;
	Pawn(int player);
	std::unordered_set<Position> get_all_legal_moves(const Board &board, const Position& position)const override;
};
