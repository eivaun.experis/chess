#include "save_manager.h"

#include <string>
#include <fstream>
#include <filesystem>
#include <ctime>

#include "hps/hps.h"
#include "chess_board_serializer.h"

std::string SaveManager::get_save_path(int i)
{
	return save_location + save_name + std::to_string(i) + save_extension;
}

bool SaveManager::save_state(const ChessBoard &board, SaveInfo &info)
{
	// Create folder to save files in
	std::filesystem::create_directories(save_location);
 
	// Get timestamp of creation
	auto now = std::chrono::system_clock::now();
	info.time_saved = std::chrono::system_clock::to_time_t(now);

	// Serialize board
	std::pair<SaveInfo, ChessBoardSerializer> data;
	data.first = info;
	data.second.set_serialization_target(&board);

	// Save board to file
	if (auto out_stream = std::ofstream(get_save_path(info.slot), std::ios::binary))
	{
		hps::to_stream(data, out_stream);
		return true;
	}
	return false;
}

bool SaveManager::load_state(ChessBoard &board, SaveInfo &info, int slot)
{
	// Read data from file and parse
	if (auto in_stream = std::ifstream(get_save_path(slot), std::ios::binary))
	{
		auto data = hps::from_stream<std::pair<SaveInfo, ChessBoardSerializer>>(in_stream);
		info = data.first;
		data.second.load_serialization_data(board);
		return true;
	}
	return false;
}

bool SaveManager::load_info(SaveInfo &info, int slot)
{
	if (auto in_stream = std::ifstream(get_save_path(slot), std::ios::binary))
	{
		info = hps::from_stream<SaveInfo>(in_stream);
		return true;
	}
	return false;
}

std::vector<SaveInfo> SaveManager::get_existing_saves()
{
	// For each possible file, check if it exists
	std::vector<SaveInfo> saves;
	for (size_t i = 0; i < 10; i++)
	{
		if (auto in_stream = std::ifstream(get_save_path(i), std::ios::binary))
		{
			SaveInfo info = hps::from_stream<SaveInfo>(in_stream);
			saves.push_back(info);
		}
	}
	return saves;
}
