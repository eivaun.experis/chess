#pragma once

#include <gtest/gtest.h>

#include "board.h"
#include "chess_pieces/pawn.h"

TEST(Pawn, Construct)
{

    Pawn pawn;
    ASSERT_EQ(pawn.get_player(), 0);

    Pawn pawn1(1);
    ASSERT_EQ(pawn1.get_player(), 1);
}

TEST(Pawn, Movement)
{
    Board board(8, 8);
    {
        Pawn pawn(ChessColor::White);
        std::unordered_set<Position> correct_movement = {{4, 5}};
        ASSERT_EQ(pawn.get_all_legal_moves(board, {4, 4}), correct_movement);
    }
    {
        Pawn pawn(ChessColor::Black);
        std::unordered_set<Position> correct_movement = {{4, 3}};
        ASSERT_EQ(pawn.get_all_legal_moves(board, {4, 4}), correct_movement);
    }
}

TEST(Pawn, FirstMove)
{
    Board board(8, 8);
    {
        Pawn pawn(ChessColor::White);
        std::unordered_set<Position> correct_movement = {{4, 2}, {4, 3}};
        ASSERT_EQ(pawn.get_all_legal_moves(board, {4, 1}), correct_movement);
    }
    {
        Pawn pawn(ChessColor::Black);
        std::unordered_set<Position> correct_movement = {{4, 5}, {4, 4}};
        ASSERT_EQ(pawn.get_all_legal_moves(board, {4, 6}), correct_movement);
    }
}

TEST(Pawn, Attack)
{
    {
        Board board(8, 8);
        Pawn pawn(ChessColor::White);
        board.add_piece<Rook>({3, 5}, ChessColor::Black);
        board.add_piece<Rook>({5, 5}, ChessColor::Black);
        std::unordered_set<Position> correct_movement = {{4, 5}, {3, 5}, {5, 5}};
        ASSERT_EQ(pawn.get_all_legal_moves(board, {4, 4}), correct_movement);
    }
    {
        Board board(8, 8);
        Pawn pawn(ChessColor::Black);
        board.add_piece<Rook>({3, 3}, ChessColor::White);
        board.add_piece<Rook>({5, 3}, ChessColor::White);
        std::unordered_set<Position> correct_movement = {{4, 3}, {3, 3}, {5, 3}};
        ASSERT_EQ(pawn.get_all_legal_moves(board, {4, 4}), correct_movement);
    }
}
