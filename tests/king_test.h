#pragma once

#include <gtest/gtest.h>

#include "board.h"
#include "chess_pieces/king.h"

TEST(King, Construct)
{
    King king;
    ASSERT_EQ(king.get_player(), 0);

    King king1(1);
    ASSERT_EQ(king1.get_player(), 1);
}

TEST(King, FreeMovement)
{
    ChessBoard board;
    Position king_pos(4, 4);
    board.add_piece<King>(king_pos, ChessColor::White);

    std::unordered_set<Position> correct_movement = {{3, 4}, {5, 4}, {4, 3}, {4, 5}, {3, 3}, {5, 3}, {3, 5}, {5, 5}};
    ASSERT_EQ(board.get_piece(king_pos)->get_all_legal_moves(board, king_pos), correct_movement);
}

TEST(King, CheckMovement)
{
    ChessBoard board;
    Position king_pos(4, 4);
    board.add_piece<King>(king_pos, ChessColor::White);

    board.add_piece<Rook>({3, 3}, ChessColor::Black);

    std::unordered_set<Position> all_movement = {{3, 5}, {4, 5}, {5, 5}, {3, 4}, {5, 4}, {3, 3}, {4, 3}, {5, 3}};
    ASSERT_EQ(board.get_piece(king_pos)->get_all_legal_moves(board, king_pos), all_movement);

    std::unordered_set<Position> correct_movement = {{5, 4}, {4, 5}, {3, 3}, {5, 5}};
    ASSERT_EQ(board.get_legal_moves(king_pos), correct_movement);
}

TEST(King, TwoKings)
{
    ChessBoard board;
    board.add_piece<King>({4, 0}, ChessColor::White);
    board.add_piece<King>({4, 7}, ChessColor::Black);

    for (int i = 0; i < 5; i++)
    {
        ASSERT_EQ(true, board.move_piece({4, i}, {4, i + 1}));
        board.set_next_player(ChessColor::White);
    }

    ASSERT_EQ(false, board.move_piece({4, 5}, {4, 6}));
}
