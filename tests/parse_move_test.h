#pragma once

#include <gtest/gtest.h>
#include "parse_move.h"

TEST(parse_move, Space)
{
	Move m;

	ASSERT_EQ(true, parse_move("a1 b2", m));
	ASSERT_EQ(0, m.start.x);
	ASSERT_EQ(0, m.start.y);
	ASSERT_EQ(1, m.goal.x);
	ASSERT_EQ(1, m.goal.y);

	ASSERT_EQ(true, parse_move("e6 d2", m));
	ASSERT_EQ(4, m.start.x);
	ASSERT_EQ(5, m.start.y);
	ASSERT_EQ(3, m.goal.x);
	ASSERT_EQ(1, m.goal.y);

	ASSERT_EQ(false, parse_move("ee c4", m));
	ASSERT_EQ(false, parse_move("e9 c4", m));
}

TEST(parse_move, NoSpace)
{
	Move m;

	ASSERT_EQ(true, parse_move("a1b2", m));
	ASSERT_EQ(0, m.start.x);
	ASSERT_EQ(0, m.start.y);
	ASSERT_EQ(1, m.goal.x);
	ASSERT_EQ(1, m.goal.y);

	ASSERT_EQ(true, parse_move("e6d2", m));
	ASSERT_EQ(4, m.start.x);
	ASSERT_EQ(5, m.start.y);
	ASSERT_EQ(3, m.goal.x);
	ASSERT_EQ(1, m.goal.y);

	ASSERT_EQ(false, parse_move("eec4", m));
	ASSERT_EQ(false, parse_move("e9c4", m));
}

TEST(parse_move, Malformed)
{
	Move m;
	
	ASSERT_EQ(false, parse_move("", m));
	ASSERT_EQ(false, parse_move("123123123123", m));
	ASSERT_EQ(false, parse_move("a1b2a1b2", m));
	ASSERT_EQ(false, parse_move(",.,.", m));
}
