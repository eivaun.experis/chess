#pragma once

#include <gtest/gtest.h>

#include "chess_pieces/rook.h"
#include "board.h"

TEST(Rook, Construct)
{

    Rook rook_1;
    ASSERT_EQ(rook_1.get_player(), 0);

    Rook rook_2(1);
    ASSERT_EQ(rook_2.get_player(), 1);
}

TEST(Rook, Movement)
{

    Board board(8, 8);
    ASSERT_EQ(board.add_piece<Rook>(Position(3, 3), 1), true);

    ASSERT_EQ(board.add_piece<Rook>(Position(3, 0), 1), true);
    ASSERT_EQ(board.add_piece<Rook>(Position(3, 6), 1), true);
    ASSERT_EQ(board.add_piece<Rook>(Position(1, 3), 1), true);
    ASSERT_EQ(board.add_piece<Rook>(Position(5, 3), 1), true);
    ASSERT_EQ(board.add_piece<Rook>(Position(3, 1), 2), true);
    ASSERT_EQ(board.add_piece<Rook>(Position(3, 5), 2), true);
    ASSERT_EQ(board.add_piece<Rook>(Position(2, 3), 2), true);
    ASSERT_EQ(board.add_piece<Rook>(Position(4, 3), 2), true);

    std::unordered_set<Position> correct_movement =
        {Position(3, 1), Position(3, 2), Position(2, 3), Position(4, 3), Position(3, 4), Position(3, 5)};

    ASSERT_EQ(board.get_legal_moves(Position(3, 3)), correct_movement);

    ASSERT_EQ(board.check_if_legal_move(Position(3, 3), Position(3, 4)), true);
    ASSERT_EQ(board.check_if_legal_move(Position(3, 3), Position(3, 6)), false);
    ASSERT_EQ(board.check_if_legal_move(Position(3, 3), Position(0, 0)), false);
    ASSERT_EQ(board.check_if_legal_move(Position(3, 3), Position(0, 8)), false);
}