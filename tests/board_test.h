#pragma once

#include <gtest/gtest.h>

#include "board.h"
#include "piece.h"
#include "position.h"
#include "chess_pieces.h"

TEST(Board, Construct)
{

    Board b(2, 2);

    ASSERT_EQ(b.get_height(), 2);
    ASSERT_EQ(b.get_width(), 2);

    ASSERT_EQ(b.get_piece(Position(0, 0)), nullptr);
    ASSERT_EQ(b.get_piece(Position(50, 50)), nullptr);

    ASSERT_EQ(b.check_if_legal_move(Position(0, 0), Position(0, 0)), false);
    ASSERT_EQ(b.check_if_legal_move(Position(10, 0), Position(0, 0)), false);

    std::unordered_set<Position> empty;
    ASSERT_EQ(b.get_legal_moves(Position(0, 0)), empty);
    ASSERT_EQ(b.get_legal_moves(Position(10, 0)), empty);

    std::vector<std::vector<std::unique_ptr<Piece>>> empty_board;
    empty_board.resize(2);
    for (auto &row : empty_board)
        row.resize(2);
    ASSERT_EQ(b.get_squares(), empty_board);

    ASSERT_EQ(b.get_player(Position(1, 1)), -1);
    try
    {
        b.get_player(Position(50, 50));
    }
    catch (const std::exception &e)
    {
        FAIL();
    }

}

TEST(Board, Adding_Pieces) {
    
    Board b(8,8);

    try
    {
        b.add_piece<Rook>(Position(0,0), 1);
        b.add_piece<Rook>(Position(7,0), 1);
        b.add_piece<Rook>(Position(0,5), 2);
    }
    catch(const std::exception& e)
    {
        FAIL();
    }
    

    ASSERT_EQ(b.get_piece(Position(0,0))->get_player(), 1);
    ASSERT_EQ(b.get_piece(Position(7,0))->get_player(), 1);
    ASSERT_EQ(b.get_piece(Position(0,5))->get_player(), 2);
}

TEST(Board, Remove) {

    Board empty_board(8,8);

    Board board(8,8);
    board.add_piece<Rook>(Position(3,3), 1);
    ASSERT_NE(board.get_piece(Position(3,3)), empty_board.get_piece(Position(3,3)));

    board.remove_piece(Position(3,3));
    ASSERT_EQ(board.get_piece(Position(3,3)), empty_board.get_piece(Position(3,3)));

}

TEST(Board, Moving) {

    Board board(8,8);

    // Dependent on Rook class to work, but it has been tested
    
    // Sucessfull movement
    board.add_piece<Rook>(Position(0,0), 1);
    board.move_piece(Position(0,0), Position(0,7));
    ASSERT_EQ(board.get_piece(Position(0,0)), nullptr);
    ASSERT_NE(board.get_piece(Position(0,7)), nullptr);

    // Failed movement
    board.move_piece(Position(0,7), Position(3,3));
    ASSERT_NE(board.get_piece(Position(0,7)), nullptr);

}