#pragma once

#include <gtest/gtest.h>
#include <unordered_set>


#include "chess_pieces.h"
#include "chess_board.h"

TEST(Queen, Movement) {
    {
        ChessBoard board;
        board.add_piece<Queen>(Position(3,3), ChessColor::White);
        std::unordered_set<Position> correct_movement = {
                // vertical
                Position(3,4),
                Position(3,5),
                Position(3,6),
                Position(3,7),
                Position(3,2),
                Position(3,1),
                Position(3,0),
                // horisontal
                Position(0,3),
                Position(1,3),
                Position(2,3),
                Position(4,3),
                Position(5,3),
                Position(6,3),
                Position(7,3),
                // diagonal up left
                Position(0,0),
                Position(1,1),
                Position(2,2),
                Position(4,4),
                Position(5,5),
                Position(6,6),
                Position(7,7),
                // diagonal down left
                Position(0,6),
                Position(1,5),
                Position(2,4),
                Position(4,2),
                Position(5,1),
                Position(6,0)
                };
        ASSERT_EQ(board.get_piece(Position(3,3))->get_all_legal_moves(board, Position(3,3)), correct_movement);
    }
    {
        ChessBoard board;
        board.add_piece<Queen>(Position(3,3), ChessColor::White);
        board.add_piece<Pawn>(Position(1,1), ChessColor::Black);
        board.add_piece<Pawn>(Position(5,5), ChessColor::White);
        std::unordered_set<Position> correct_movement = {
                // vertical
                Position(3,4),
                Position(3,5),
                Position(3,6),
                Position(3,7),
                Position(3,2),
                Position(3,1),
                Position(3,0),
                // horisontal
                Position(0,3),
                Position(1,3),
                Position(2,3),
                Position(4,3),
                Position(5,3),
                Position(6,3),
                Position(7,3),
                // diagonal up left
                Position(1,1),
                Position(2,2),
                Position(4,4),
                // diagonal down left
                Position(0,6),
                Position(1,5),
                Position(2,4),
                Position(4,2),
                Position(5,1),
                Position(6,0)
                };
        ASSERT_EQ(board.get_piece(Position(3,3))->get_all_legal_moves(board, Position(3,3)), correct_movement);
    }
}