#pragma once

#include <fstream>
#include <gtest/gtest.h>

#include "chess_board_serializer.h"
#include "chess_pieces.h"

TEST(ChessBoardSerializer, SaveLoadAll)
{
    {
        ChessBoard b;
        b.set_next_player(ChessColor::Black);
        b.add_piece<Pawn>({0, 1}, 1);
        b.add_piece<Rook>({2, 3}, 2);
        b.add_piece<Knight>({4, 5}, 3);
        b.add_piece<Bishop>({1, 2}, 4);
        b.add_piece<Queen>({3, 4}, 5);
        b.add_piece<King>({5, 6}, 6);

        b.get_white().clear();
        b.get_white().insert({123, 456});

        ChessBoardSerializer s;
        s.set_serialization_target(&b);
        auto out_stream = std::ofstream("ChessBoardSerializerTest.bin", std::ios::binary);
        hps::to_stream(s, out_stream);
    }
    {
        auto in_stream = std::ifstream("ChessBoardSerializerTest.bin", std::ios::binary);
        ChessBoardSerializer s = hps::from_stream<ChessBoardSerializer>(in_stream);
        in_stream.close();

        ChessBoard b;
        s.load_serialization_data(b);

        ASSERT_EQ(ChessColor::Black, b.get_next_player());
        ASSERT_EQ(1, b.get_white().size());
        ASSERT_EQ(Position(123, 456), *b.get_white().begin());

        for (int y = 0; y < b.get_height(); y++)
        {
            for (int x = 0; x < b.get_width(); x++)
            {
                Position pos(x, y);
                if (pos == Position(0, 1))
                {
                    ASSERT_EQ(1, b.get_player(pos));
                    ASSERT_NE(nullptr, dynamic_cast<Pawn *>(b.get_piece(pos)));
                }
                else if (pos == Position(2, 3))
                {
                    ASSERT_EQ(2, b.get_player(pos));
                    ASSERT_NE(nullptr, dynamic_cast<Rook *>(b.get_piece(pos)));
                }
                else if (pos == Position(4, 5))
                {
                    ASSERT_EQ(3, b.get_player(pos));
                    ASSERT_NE(nullptr, dynamic_cast<Knight *>(b.get_piece(pos)));
                }
                else if (pos == Position(1, 2))
                {
                    ASSERT_EQ(4, b.get_player(pos));
                    ASSERT_NE(nullptr, dynamic_cast<Bishop *>(b.get_piece(pos)));
                }
                else if (pos == Position(3, 4))
                {
                    ASSERT_EQ(5, b.get_player(pos));
                    ASSERT_NE(nullptr, dynamic_cast<Queen *>(b.get_piece(pos)));
                }
                else if (pos == Position(5, 6))
                {
                    ASSERT_EQ(6, b.get_player(pos));
                    ASSERT_NE(nullptr, dynamic_cast<King *>(b.get_piece(pos)));
                }
                else
                {
                    ASSERT_EQ(-1, b.get_player(pos));
                    ASSERT_EQ(nullptr, b.get_piece({x, y}));
                }
            }
        }
    }
}
