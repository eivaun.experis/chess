#include <gtest/gtest.h>

// Add tests here
#include "position_test.h"
#include "board_test.h"
#include "serializer_test.h"
#include "parse_move_test.h"
#include "chess_board_test.h"

#include "pawn_test.h"
#include "rook_test.h"
#include "bishop_test.h"
#include "king_test.h"
#include "queen_test.h"
#include "knight_test.h"

#include "play_test.h"

int main(int argc, char **argv)
{
    // Pass through any arguments that we specify
    testing::InitGoogleTest(&argc, argv);

    // Run the tests
    return RUN_ALL_TESTS();
}
