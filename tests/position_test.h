#pragma once

#include <gtest/gtest.h>

#include "position.h"

TEST(Position, Construct)
{
    Position p;
    ASSERT_EQ(0, p.x);
    ASSERT_EQ(0, p.y);

    Position p2(10, 20);
    ASSERT_EQ(10, p2.x);
    ASSERT_EQ(20, p2.y);
}

TEST(Position, Equals)
{
    Position p(10, 20);
    Position p2(10, 20);
    ASSERT_EQ(p, p2);

    Position p3(20, 10);
    ASSERT_NE(p, p3);
}

TEST(Position, Arithmetic)
{
    Position p(10, 20);
    Position p2(5, 7);

    Position p3 = p + p2;
    ASSERT_EQ(15, p3.x);
    ASSERT_EQ(27, p3.y);

    p3 = p - p2;
    ASSERT_EQ(5, p3.x);
    ASSERT_EQ(13, p3.y);

    p += {50, 30};
    ASSERT_EQ(60, p.x);
    ASSERT_EQ(50, p.y);

    p -= {5, 7};
    ASSERT_EQ(55, p.x);
    ASSERT_EQ(43, p.y);
}
