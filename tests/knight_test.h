#pragma once

#include <gtest/gtest.h>
#include <unordered_set>

#include "chess_pieces.h"

TEST(Knight, FreeMovement)
{
    ChessBoard board;
    board.add_piece<Knight>({3, 3}, ChessColor::White);
    std::unordered_set<Position> correct_movement = {
        {2, 5},
        {4, 5},
        {1, 4},
        {5, 4},
        {1, 2},
        {5, 2},
        {2, 1},
        {4, 1},
    };

    ASSERT_EQ(board.get_piece({3, 3})->get_all_legal_moves(board, {3, 3}), correct_movement);
}

TEST(Knight, CornerMovement)
{
    {
        ChessBoard board;
        board.add_piece<Knight>({0, 0}, ChessColor::White);

        std::unordered_set<Position> correct_movement = {{1, 2}, {2, 1}};
        ASSERT_EQ(board.get_piece({0, 0})->get_all_legal_moves(board, {0, 0}), correct_movement);
    }

    {
        ChessBoard board;
        board.add_piece<Knight>({7, 7}, ChessColor::White);

        std::unordered_set<Position> correct_movement = {{6, 5}, {5, 6}};
        ASSERT_EQ(board.get_piece({7, 7})->get_all_legal_moves(board, {7, 7}), correct_movement);
    }
}

TEST(Knight, BlockedMovement)
{
    ChessBoard board;
    board.add_piece<Knight>({3, 3}, ChessColor::White);
    board.add_piece<Pawn>({2, 5}, ChessColor::White);
    board.add_piece<Pawn>({4, 5}, ChessColor::Black);

    std::unordered_set<Position> correct_movement = {
        {4, 5},
        {1, 4},
        {5, 4},
        {1, 2},
        {5, 2},
        {2, 1},
        {4, 1},
    };

    ASSERT_EQ(board.get_piece({3, 3})->get_all_legal_moves(board, {3, 3}), correct_movement);
}