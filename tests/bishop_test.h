#pragma once

#include <gtest/gtest.h>

#include "board.h"
#include "chess_pieces/bishop.h"

TEST(Bishop, Construct)
{

    Bishop bishop;
    ASSERT_EQ(bishop.get_player(), 0);

    Bishop bishop1(1);
    ASSERT_EQ(bishop1.get_player(), 1);
}

TEST(Bishop, Corner00)
{
    Board board(8, 8);

    Bishop bishop(ChessColor::White);
    std::unordered_set<Position> correct_movement = {{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}, {6, 6}, {7, 7}};
    auto moves = bishop.get_all_legal_moves(board, {0, 0});
    ASSERT_EQ(moves.size(), correct_movement.size());
    ASSERT_EQ(moves, correct_movement);
}

TEST(Bishop, Corner77)
{
    Board board(8, 8);

    Bishop bishop(ChessColor::White);
    std::unordered_set<Position> correct_movement = {{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}, {6, 6}};
    auto moves = bishop.get_all_legal_moves(board, {7, 7});
    ASSERT_EQ(moves.size(), correct_movement.size());
    ASSERT_EQ(moves, correct_movement);
}

TEST(Bishop, Corner07)
{
    Board board(8, 8);

    Bishop bishop(ChessColor::White);
    std::unordered_set<Position> correct_movement = {{1, 6}, {2, 5}, {3, 4}, {4, 3}, {5, 2}, {6, 1}, {7, 0}};
    auto moves = bishop.get_all_legal_moves(board, {0, 7});
    ASSERT_EQ(moves.size(), correct_movement.size());
    ASSERT_EQ(moves, correct_movement);
}

TEST(Bishop, Corner70)
{
    Board board(8, 8);

    Bishop bishop(ChessColor::White);
    std::unordered_set<Position> correct_movement = {{0, 7}, {1, 6}, {2, 5}, {3, 4}, {4, 3}, {5, 2}, {6, 1}};
    auto moves = bishop.get_all_legal_moves(board, {7, 0});
    ASSERT_EQ(moves.size(), correct_movement.size());
    ASSERT_EQ(moves, correct_movement);
}

TEST(Bishop, FullMovement)
{
    Board board(8, 8);

    Bishop bishop(ChessColor::White);
    std::unordered_set<Position> correct_movement = {{0, 0}, {1, 1}, {2, 2}, {3, 3}, {5, 5}, {6, 6}, {7, 7}, {1, 7}, {2, 6}, {3, 5}, {5, 3}, {6, 2}, {7, 1}};
    auto moves = bishop.get_all_legal_moves(board, {4, 4});
    ASSERT_EQ(moves.size(), correct_movement.size());
    ASSERT_EQ(moves, correct_movement);
}
