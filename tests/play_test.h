#pragma once

#include <gtest/gtest.h>

#include "chess_pieces.h"
#include "chess_board.h"
#include "parse_move.h"

TEST(Play, FoolsMate)
{
    Move move;
    ChessBoard board;
    board.setup_board();

    ASSERT_EQ(parse_move("f2f3", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("e7e5", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("g2g4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d8h4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), true);
}

TEST(Play, FoolsMate2)
{
    Move move;
    ChessBoard board;
    board.setup_board();

    ASSERT_EQ(parse_move("e2e4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("f7f6", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d2d4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("g7g5", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d1h5", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), true);
}

TEST(Play, GrobsAttack)
{
    Move move;
    ChessBoard board;
    board.setup_board();

    ASSERT_EQ(parse_move("g2g4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("e7e5", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("f2f4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d8h4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), true);
}

TEST(Play, BirdsOpening)
{
    Move move;
    ChessBoard board;
    board.setup_board();

    ASSERT_EQ(parse_move("f2f4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("e7e5", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("f4e5", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d7d6", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("e5d6", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("f8d6", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("b1c3", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d8h4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_check(board.get_next_player()), true);

    ASSERT_EQ(parse_move("g2g3", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_check(board.get_next_player()), false);

    ASSERT_EQ(parse_move("h4g3", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_check(board.get_next_player()), true);

    ASSERT_EQ(parse_move("h2g3", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d6g3", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), true);
}

TEST(Play, SmotheredMate)
{
    Move move;
    ChessBoard board;
    board.setup_board();

    ASSERT_EQ(parse_move("e2e4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("c7c6", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d2d4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d7d5", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("b1c3", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d5e4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("c3e4", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("b8d7", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("d1e2", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("g8f6", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), false);

    ASSERT_EQ(parse_move("e4d6", move), true);
    ASSERT_EQ(board.move_piece(move.start, move.goal), true);
    ASSERT_EQ(board.is_checkmate(board.get_next_player()), true);
}
