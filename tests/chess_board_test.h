#pragma once

#include <gtest/gtest.h>

#include "board.h"
#include "position.h"
#include "chess_board.h"
#include "chess.h"
#include "chess_pieces.h"

TEST(ChessBoard, Castling_success)
{
    // King-side
    ChessBoard board;

    board.add_piece<Rook>(Position(7, 0), ChessColor::White);
    board.add_piece<King>(Position(4, 0), ChessColor::White);

    ASSERT_EQ(board.move_piece(Position(4, 0), Position(6, 0)), true);
    ASSERT_EQ(get_piece_enum(board.get_piece(Position(6, 0))), ChessPiece::King);
    ASSERT_EQ(get_piece_enum(board.get_piece(Position(5, 0))), ChessPiece::Rook);
    ASSERT_EQ(board.get_piece(Position(4, 0)), nullptr);
    ASSERT_EQ(board.get_piece(Position(7, 0)), nullptr);

    // // Queen-side
    ChessBoard board2;

    board2.add_piece<Rook>(Position(0, 0), ChessColor::White);
    board2.add_piece<King>(Position(4, 0), ChessColor::White);

    ASSERT_EQ(board2.move_piece(Position(4, 0), Position(1, 0)), true);
    ASSERT_EQ(get_piece_enum(board2.get_piece(Position(1, 0))), ChessPiece::King);
    ASSERT_EQ(get_piece_enum(board2.get_piece(Position(2, 0))), ChessPiece::Rook);
    ASSERT_EQ(board2.get_piece(Position(0, 0)), nullptr);
    ASSERT_EQ(board2.get_piece(Position(4, 0)), nullptr);
}

TEST(ChessBoard, Castling_fail)
{
    ChessBoard board;

    board.add_piece<Rook>(Position(7, 0), ChessColor::White);
    board.add_piece<King>(Position(4, 0), ChessColor::White);

    ASSERT_EQ(board.move_piece(Position(4, 0), Position(5, 0)), true);
    board.set_next_player(ChessColor::White);
    ASSERT_EQ(board.move_piece(Position(5, 0), Position(4, 0)), true);
    board.set_next_player(ChessColor::White);

    ASSERT_EQ(board.move_piece(Position(4, 0), Position(6, 0)), false);
    ASSERT_EQ(board.get_piece(Position(6, 0)), nullptr);
    ASSERT_EQ(board.get_piece(Position(5, 0)), nullptr);
    ASSERT_EQ(get_piece_enum(board.get_piece(Position(4, 0))), ChessPiece::King);
    ASSERT_EQ(get_piece_enum(board.get_piece(Position(7, 0))), ChessPiece::Rook);
}

TEST(ChessBoard, En_passant_success)
{
    // Black takes white
    ChessBoard board;
    ASSERT_EQ(board.add_piece<Pawn>(Position(0, 1), ChessColor::White), true);
    ASSERT_EQ(board.add_piece<Pawn>(Position(1, 3), ChessColor::Black), true);
    ASSERT_EQ(board.move_piece(Position(0, 1), Position(0, 3)), true);
    ASSERT_EQ(board.move_piece(Position(1, 3), Position(0, 2)), true);

    // White takes black
    ChessBoard board2;
    ASSERT_EQ(board2.add_piece<Pawn>(Position(5, 6), ChessColor::Black), true);
    ASSERT_EQ(board2.add_piece<Pawn>(Position(6, 4), ChessColor::White), true);

    ASSERT_EQ(board2.add_piece<Pawn>(Position(6, 1), ChessColor::White), true);
    ASSERT_EQ(board2.move_piece(Position(6, 1), Position(6, 2)), true);

    ASSERT_EQ(board2.move_piece(Position(5, 6), Position(5, 4)), true);
    ASSERT_EQ(board2.move_piece(Position(6, 4), Position(5, 5)), true);
}

TEST(ChessBoard, En_passant_fail)
{
    ChessBoard board;

    ASSERT_EQ(board.add_piece<Pawn>(Position(0, 6), ChessColor::Black), true);
    ASSERT_EQ(board.add_piece<Pawn>(Position(1, 4), ChessColor::White), true);
    ASSERT_EQ(board.add_piece<Pawn>(Position(6, 6), ChessColor::Black), true);
    ASSERT_EQ(board.add_piece<Pawn>(Position(6, 1), ChessColor::White), true);

    ASSERT_EQ(board.move_piece(Position(6, 1), Position(6, 2)), true);
    ASSERT_EQ(board.move_piece(Position(0, 6), Position(0, 4)), true);
    ASSERT_EQ(board.move_piece(Position(6, 2), Position(6, 3)), true);

    ASSERT_EQ(board.move_piece(Position(6, 6), Position(6, 5)), true);
    ASSERT_EQ(board.move_piece(Position(1, 4), Position(0, 5)), false);
}

TEST(ChessBoard, Check)
{
    ChessBoard b;
    ChessColor defender = ChessColor::White;

    ASSERT_EQ(b.is_check(defender), false);

    b.add_piece<King>({4, 4}, ChessColor::White);
    ASSERT_EQ(b.is_check(defender), false);

    b.add_piece<Rook>({4, 1}, ChessColor::White);
    ASSERT_EQ(b.is_check(defender), false);

    b.add_piece<Rook>({4, 0}, ChessColor::Black);
    ASSERT_EQ(b.is_check(defender), false);

    b.add_piece<Rook>({0, 4}, ChessColor::Black);
    ASSERT_EQ(b.is_check(defender), true);

    b.add_piece<Pawn>({1, 4}, ChessColor::White);
    ASSERT_EQ(b.is_check(defender), false);
}

TEST(ChessBoard, Checkmate1)
{
    ChessBoard b;
    ChessColor defender = ChessColor::White;

    ASSERT_EQ(b.is_checkmate(defender), false);

    b.add_piece<King>({4, 4}, ChessColor::White);
    ASSERT_EQ(b.is_checkmate(defender), false);

    b.add_piece<Rook>({4, 1}, ChessColor::White);
    ASSERT_EQ(b.is_checkmate(defender), false);

    b.add_piece<Rook>({4, 0}, ChessColor::Black);
    ASSERT_EQ(b.is_checkmate(defender), false);

    b.add_piece<Rook>({0, 4}, ChessColor::Black);
    ASSERT_EQ(b.is_checkmate(defender), false);

    b.add_piece<Pawn>({1, 4}, ChessColor::White);
    ASSERT_EQ(b.is_checkmate(defender), false);
}

TEST(ChessBoard, Checkmate2)
{
    ChessBoard b;
    ChessColor defender = ChessColor::White;

    b.add_piece<King>({0, 0}, ChessColor::White);
    b.add_piece<Rook>({7, 0}, ChessColor::Black);
    ASSERT_EQ(b.is_checkmate(defender), false);
    b.add_piece<Rook>({7, 1}, ChessColor::Black);
    ASSERT_EQ(b.is_checkmate(defender), true);
}

TEST(ChessBoard, Checkmate3)
{
    ChessBoard b;
    ChessColor defender = ChessColor::White;

    b.add_piece<King>({0, 0}, ChessColor::White);
    b.add_piece<Rook>({7, 1}, ChessColor::Black);
    b.add_piece<Rook>({7, 2}, ChessColor::Black);
    ASSERT_EQ(b.is_checkmate(defender), false);
}

TEST(ChessBoard, Checkmate4)
{
    ChessBoard b;
    ChessColor defender = ChessColor::White;

    b.add_piece<King>({0, 0}, ChessColor::White);
    b.add_piece<Rook>({7, 0}, ChessColor::Black);
    ASSERT_EQ(b.is_checkmate(defender), false);
}

TEST(ChessBoard, Checkmate5)
{
    ChessBoard b;
    ChessColor defender = ChessColor::White;
    b.set_next_player(defender);

    b.add_piece<King>({4, 0}, ChessColor::White);
    b.add_piece<Queen>({4, 1}, ChessColor::Black);
    b.add_piece<Queen>({5, 1}, ChessColor::Black);
    b.add_piece<Queen>({4, 2}, ChessColor::Black);

    ASSERT_EQ(b.is_checkmate(defender), true);
}

TEST(ChessBoard, Checkmate5_is_stalemate)
{
    ChessBoard b;
    ChessColor defender = ChessColor::Black;
    b.set_next_player(defender);

    b.add_piece<King>({4, 0}, ChessColor::White);
    b.add_piece<Pawn>({4, 1}, ChessColor::Black);
    b.add_piece<Pawn>({5, 2}, ChessColor::Black);
    b.add_piece<Pawn>({4, 2}, ChessColor::Black);
    ASSERT_EQ(b.is_checkmate(defender), false);
}

TEST(ChessBoard, Stalemate1)
{
    ChessBoard b;
    b.set_next_player(ChessColor::White);

    b.add_piece<King>({4, 0}, ChessColor::White);
    b.add_piece<Pawn>({4, 1}, ChessColor::Black);
    b.add_piece<Pawn>({5, 2}, ChessColor::Black);
    b.add_piece<Pawn>({4, 2}, ChessColor::Black);
    ASSERT_EQ(b.is_stalemate(), true);
}

TEST(ChessBoard, Stalemate1_wrong_player)
{
    ChessBoard b;
    b.set_next_player(ChessColor::Black);

    b.add_piece<King>({4, 0}, ChessColor::White);
    b.add_piece<Pawn>({4, 1}, ChessColor::Black);
    b.add_piece<Pawn>({5, 2}, ChessColor::Black);
    b.add_piece<Pawn>({4, 2}, ChessColor::Black);
    ASSERT_EQ(b.is_stalemate(), false);
}

TEST(ChessBoard, Promotion) {
    
    ChessBoard board;
    board.set_promotion_prompt([](){return ChessPiece::Queen;});

    ASSERT_EQ(board.add_piece<Pawn>(Position(0,6), ChessColor::White), true);
    ASSERT_EQ(board.move_piece(Position(0,6), Position(0,7)), true);

    ASSERT_EQ(get_piece_enum(board.get_piece(Position(0,7))), ChessPiece::Queen);
}