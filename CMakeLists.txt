# Set the minimum cmake version
cmake_minimum_required(VERSION 3.10)

# Set the project name and version
project(chess VERSION 1.0)

# Add the source and dependencies folders to the include path
include_directories(src dependencies)

# Add the library
add_library(chess_lib
	src/position.cpp
	src/board.cpp
	src/piece.cpp
	src/chess_board.cpp
	src/chess_board_serializer.cpp
	src/chess_pieces/pawn.cpp
	src/chess_pieces/rook.cpp
	src/chess_pieces/knight.cpp
	src/chess_pieces/bishop.cpp
	src/chess_pieces/queen.cpp
	src/chess_pieces/king.cpp
	src/printer.cpp
	src/save_manager.cpp
	src/parse_move.cpp
	src/menu.cpp
)

# Add the executable
add_executable(chess src/main.cpp)
target_link_libraries(chess chess_lib)

# Enable testing frameworks
enable_testing()

# Locate GTest and include it
find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

# Link tests with what we want to test and the GoogleTest and pthread library
add_executable(tests tests/main.cpp)
target_link_libraries(tests chess_lib ${GTEST_LIBRARIES} pthread)

# Tests
gtest_add_tests(TARGET tests SOURCES 
	tests/position_test.h
	tests/board_test.h
	tests/serializer_test.h
	tests/chess_board_test.h
	tests/parse_move_test.h
	tests/rook_test.h
	tests/pawn_test.h
	tests/bishop_test.h
	tests/king_test.h
	tests/knight_test.h
	tests/queen_test.h
	tests/play_test.h
)

# Specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# Make release the default build
if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Release)
	message(STATUS "Build type not specified: Use Release by default")
endif()

# Set compiler flags
set(CMAKE_CXX_FLAGS "-Wall -Wextra --coverage")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

message(STATUS "Build type: ${CMAKE_BUILD_TYPE}")
