# Build
FROM gcc:11 as builder

RUN apt-get update --yes
RUN apt-get install --yes cmake
RUN apt-get install --yes libgtest-dev

RUN mkdir -p /usr/chess
COPY . /usr/chess/

RUN cd /usr/chess; \ 
    cmake -B ./build; \
    make -C ./build
    
ENTRYPOINT ["/usr/chess/build/chess"]
