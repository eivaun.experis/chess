# Chess

## Overview

This is a program which lets two players play chess against each other!  
It supports all features of chess, including (but not limited to) promotion, castling and en passant.  
The program has 10 save slots which you can use to save a game in progress, and a move replayer to recap how your bad decisions made you end up in this losing position.

## Dependencies

**CMake**

The project uses the CMake build system. For install instructions see https://cmake.org/

**GoogleTest** 

GoogleTest is used for unit testing. It can be installed with `sudo apt-get install libgtest-dev`

**HPS** 

HPS is used for serialization. This library is included in the repo.

## Build

The project is built using CMake with the provided `CMakeLists.txt`

**VS Code**

This requires the CMake extension for VS Code.\
Configure: `Cmake: Configure` \
Compile: `CMake: Build`

**Terminal**

Configure: `cmake -B build` \
Compile: `make -C build`

## Usage

The program can be launced from the root folder of the project using the command: `./build/chess`

When running the application, you get prompted to choose between different display-modes.

```
Unicode:     ♟ ♜ ♞ ♝ ♛ ♚ ♙ ♖ ♘ ♗ ♕ ♔
Not Unicode: wP wR wN wB wQ wK bP bR bN bB bQ bK
```
If you select Unicode, you get to choose whether it looks better with an extra space inserted after the Unicode characters.

```
Space:    |♟ ||♜ ||♞ ||♝ ||♛ ||♚ |
No space: |♟| |♜| |♞| |♝| |♛| |♚|
```
The gameboard has the indices `a-h` on the x-axis and `1-8` on the y-axis.
```
╔══╤══╤══╤══╤══╤══╤══╤══╗
║  │  │  │  │  │  │  │  ║ 8
╟──┼──┼──┼──┼──┼──┼──┼──╢
║  │  │  │  │  │  │  │  ║ 7
╟──┼──┼──┼──┼──┼──┼──┼──╢
║  │  │  │  │  │  │  │  ║ 6
╟──┼──┼──┼──┼──┼──┼──┼──╢
║  │  │  │  │  │  │  │  ║ 5
╟──┼──┼──┼──┼──┼──┼──┼──╢
║  │  │  │  │  │  │  │  ║ 4
╟──┼──┼──┼──┼──┼──┼──┼──╢
║  │  │  │  │  │  │  │  ║ 3
╟──┼──┼──┼──┼──┼──┼──┼──╢
║  │  │  │  │  │  │  │  ║ 2
╟──┼──┼──┼──┼──┼──┼──┼──╢
║  │  │  │  │  │  │  │  ║ 1
╚══╧══╧══╧══╧══╧══╧══╧══╝
 a  b  c  d  e  f  g  h 
```
To enter a move, use the notation "x<sub>1</sub>y<sub>1</sub> x<sub>2</sub>y<sub>2</sub>". The space is optional. \
E.g. `e6 c4` to move from `e6` to `c4`, and `a2a4` to move from `a2` to `a4`. \
You can also enter `exit` to exit or `concede` to concede the game. \
The game autosaves after every move.

## Usage (precompiled Docker image)
A precompiled version of the program can be run using [Docker](https://www.docker.com/).
```
docker pull registry.gitlab.com/eivaun.experis/chess; docker run -it registry.gitlab.com/eivaun.experis/chess:latest
```

## Contributors

Alexander Øvergård
- Pawn, King
- Move recorder, Replay function
- Notation parser
- Parts of save manager

Eivind Vold Aunebakk
- Pawn, King, Bishop
- Serialization
- Menu, Input
- Position
- Board
- Check, checkmate, stalemate
- Tests, CI

Lucas Emmes  
- Rook, Bishop, Queen, Knight  
- Castling, En passant, Promotion  
- Board printer  
- Unit testing of above  
